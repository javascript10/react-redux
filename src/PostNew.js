import React, { PureComponent as Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import {
  createPost,
  getPostsLists,
} from "./redux/actions/postActions";

class PostNew extends Component {
  addPost = () => {
    const { getPostsLists } = this.props;
    getPostsLists()
      .then(posts => {
        console.log("data");
        console.log(posts);
      })
      .catch(err => console.log("eeror", err));
  };

  render() {
    return (
      <div
        role="button"
        tabIndex="0"
        onClick={this.addPost}
        onKeyDown={() => {}}
      >
        hey
      </div>
    );
  }
}

PostNew.propTypes = {
  createPost: PropTypes.func.isRequired,
  getPostsLists: PropTypes.func.isRequired,
};

const mapStateToProps = state => {
  console.log(state);
  return {
    posts: state.postComponent.posts,
  };
};

const mapDispatchToProps = {
  createPost,
  getPostsLists,
};

export default connect(mapStateToProps, mapDispatchToProps)(PostNew);
