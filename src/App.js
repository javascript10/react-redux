import React from "react";
import Posts from "./Posts";
import PostNew from "./PostNew";

const App = () => (
  <div>
    Demo
    <Posts title="title" />
    <PostNew />
  </div>
);

export default App;
