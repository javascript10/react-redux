import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import {
  createPost,
  getPostsLists,
} from "./redux/actions/postActions";

const Posts = props => {
  return (
    <div
      role="button"
      tabIndex="0"
      onClick={() => props.getPostsLists({ user: 2 })}
      onKeyDown={() => {}}
    >
      hi
    </div>
  );
};

Posts.propTypes = {
  createPost: PropTypes.func.isRequired,
  getPostsLists: PropTypes.func.isRequired,
};

const mapStateToProps = state => {
  return {
    posts: state.postComponent.posts,
  };
};

const mapDispatchToProps = {
  createPost,
  getPostsLists,
};

export default connect(mapStateToProps, mapDispatchToProps)(Posts);
