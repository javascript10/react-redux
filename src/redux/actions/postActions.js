import Axios from "axios";
import actionCreator, {
  CREATE_POST,
  DELETE_POST,
  UPDATE_POST,
} from "./actionUtils";

export const createPost = post => actionCreator(CREATE_POST, post);
export const updatePost = post => actionCreator(UPDATE_POST, post);
export const deletePost = post => actionCreator(DELETE_POST, post);

export const getPostsLists = () => {
  return (dispatch, getState) => {
    return new Promise((res, rej) => {
      Axios.get("https://jsonplaceholder.typicode.com/posts")
        .then(posts => {
          dispatch(createPost(posts.data));
          res(posts);
        })
        .catch(err => {
          rej(err);
        });
    });
  };
};

export const getPostsLists1 = () => {
  return (dispatch, getState) => {
    Axios.get("https://jsonplaceholder.typicode.com/posts")
      .then(posts => {
        dispatch(createPost(posts.data));
      })
      .catch(err => {
        throw err;
      });
  };
};

const postActions = {
  createPost,
  updatePost,
  deletePost,
  getPostsLists,
};

export default postActions;
