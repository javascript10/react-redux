import { combineReducers } from "redux";

import postReducer from "./postReducer";

const rootReducer = combineReducers({ postComponent: postReducer });

export default rootReducer;
