import {
  CREATE_POST,
  UPDATE_POST,
  GET_POSTS,
} from "../actions/actionUtils";

const postInitialState = {
  posts: [],
};

const postReducer = (state = postInitialState, action) => {
  switch (action.type) {
    case CREATE_POST:
      return { ...state, posts: [...state.posts, action.payload] };
    case UPDATE_POST:
      return state;
    case GET_POSTS:
      return state;
    default:
      return state;
  }
};

export default postReducer;
